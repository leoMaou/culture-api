# culture-api

An api to manage cultural content (animations / mangas)

## MongoDb

```bash
docker run -d --restart unless-stopped --name=mongodb \
 -p 27017:27017 \
 --log-opt max-size=10m --log-opt max-file=5 \
 -e MONGO_INITDB_ROOT_USERNAME=root \
 -e MONGO_INITDB_ROOT_PASSWORD=EBtW9qHepvdw4aAb \
 mongo
```

### Connecting with terminal

```bash
docker exec -it mongodb /bin/bash
mongo -u root -p --authenticationDatabase admin
```

### Creating user

[create database and user with mongo shell](https://www.shellhacks.com/mongodb-create-database-and-user-mongo-shell/)

```bash
use cutureapi

db.initial_collection.insert({ initial: "initial_value" })

db.createUser(
{
user: "cultureapi",
pwd: "LKIGFD432FDSTJ",
roles: ["readWrite"]
}
)

```

## Documentation

https://documenter.getpostman.com/view/6329680/SztBc92Z?version=latest