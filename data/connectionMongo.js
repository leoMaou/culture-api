const mongoose = require('mongoose');

var url = 'mongodb://cultureapi:LKIGFD432FDSTJ@localhost:27017/cutureapi';
var options = {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false};

mongoose.connect(url, options);

mongoose.connection.on('connected', () => {
    console.log("MongoDB connected")
});

mongoose.connection.on('error', error => {
    console.log("Error connecting to the MongoDB, cause: ", error);
});

module.exports = mongoose;
