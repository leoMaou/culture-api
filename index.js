const express = require('express');
const cors = require('cors');
const app = express();
const jwt = require('jsonwebtoken');
const usuario = require('./routes/UsuarioRoute');
const personagem = require('./routes/PersonagemRoute');
const obra = require('./routes/ObraRoute');
const dublador = require('./routes/DubladorRoute');
const autor = require('./routes/AutorRoute');

var SECRET = "AFDJDKLGHSLHJFLKDSLKFDKLHGFDS3421N";

app.use(cors())
app.use(express.json());
app.use((req, res, next) => {
    if(req.url === '/api-docs') {
        next();
    }else {
        // regex para permitir qualquer rota /usuario/* , nesse caso só as rotas atualizar, salvar, buscar por id e deletar usuario são permitidas sem autenticação
        let regex = /(\/usuarios\/)([a-zA-Z0-9]+)/g;
        let responseRegex = regex.exec(req.url);

        //response == null caso não exista match
        if (responseRegex != null) {
            next();
        } else {
            var token = req.headers['x-access-token'];

            if (token) {
                jwt.verify(token, SECRET, function (err, decoded) {
                    if (err) {
                        return res.status(500).send({auth: false, message: 'Failed to authenticate token.'});
                    }
                    console.log("Have permission");
                    next();
                });
            }else {
                console.log("Permission Denied");
                res.status(502).json({error: `permission denied`});
            }
        }
    }
});

app.use('/usuarios', usuario);
app.use('/personagens', personagem);
app.use('/obras', obra);
app.use('/dubladores', dublador);
app.use('/autores', autor);

app.listen("8080", () => {
    console.log("cuture-api started on port 8080");
});
