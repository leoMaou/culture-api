const mongoose = require('../data/connectionMongo');

var autorScheme = mongoose.Schema(
    {
        nome: String,
        fotoUrl: String,
        genero: String,
        obras: [{ type: mongoose.ObjectId, ref: 'obras' }]
    });

var Autor = mongoose.model('autores', autorScheme);

module.exports = Autor;
