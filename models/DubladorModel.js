const mongoose = require('../data/connectionMongo');

var dubladorScheme = mongoose.Schema(
    {
        nome: String,
        descricao: String,
        fotoUrl: String,
        genero: String,
        personagens : [{ type: mongoose.ObjectId, ref: 'Personagens' }]
    });

var Dublador = mongoose.model('dubladores', dubladorScheme);

module.exports = Dublador;
