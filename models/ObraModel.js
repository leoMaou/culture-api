const mongoose = require('../data/connectionMongo');

var obraScheme = mongoose.Schema(
    {
        nome: String,
        descricao: String,
        fotoUrl: String,
        status: String,
        source: String,
        generos: [{type: String}],
        formato: String,
        studios: [{type: String}],
    });

var Obra = mongoose.model('obras', obraScheme);

module.exports = Obra;
