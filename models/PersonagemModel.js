const mongoose = require('../data/connectionMongo');

var personagemScheme = mongoose.Schema(
    {
        nome: String,
        descricao: String,
        fotoUrl: String,
        genero: String,
        obras: [{ type: mongoose.ObjectId, ref: 'obras' }]
    });

var Personagem = mongoose.model('personagens', personagemScheme);

module.exports = Personagem;
