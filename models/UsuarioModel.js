const mongoose = require('../data/connectionMongo');

var usuarioScheme = mongoose.Schema(
    {
        nome: String,
        login: String,
        senha: String,
        criadoEm: {type: Date, default: Date.now},
        favoritos: [{ type: mongoose.ObjectId, ref: 'obras' }]
    });

var Usuario = mongoose.model('usuarios', usuarioScheme);

module.exports = Usuario;
