const express = require('express');
const router = express.Router();
const Autor = require('../models/AutorModel');


router.get("/", (req, res) => {
    var skip = 0;
    var limit = 10;
    var filter = {};

    if (req.query.skip) {
        skip = Number(req.query.skip);
    }

    if (req.query.limit) {
        limit = Number(req.query.limit);
    }

    if (req.query.nome) {
        filter.nome = {$regex: req.query.nome, $options: 'i'};
    }

    if (req.query.fotoUrl) {
        filter.fotoUrl = req.query.fotoUrl;
    }

    if (req.query.genero) {
        filter.genero = req.query.genero;
    }

    Autor.find(filter, 'nome fotoUrl genero obras', {skip: skip, limit: limit}, (error, docs) => {

        if (error) {
            res.status(400).send({error: `Could not find the authors. cause: ${error}`});
            return;
        }

        if (docs == null) {
            res.status(400).send({error: `Could not find the authors. cause: author not found`});
        } else {
            res.json(docs);
        }
    });
});

router.get("/:id", (req, res) => {
    var autorId = req.params.id;

    if (!autorId) {
        res.status(502).send({error: `id is required`});
        return;
    }

    Autor.findById(autorId, (error, doc) => {
        if (error) {
            res.status(400).send({error: `Could not find the author with id ${autorId} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `Could not find the author with id ${autorId} . cause: author not found`});
        } else {
            res.json(doc);
        }
    });
});

router.delete("/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).json({error: `id is required`})
        return;
    }

    Autor.findOneAndDelete({_id: id}, function (error, doc) {

        if (error) {
            res.status(400).send({error: `Could not delete the author with id ${id} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `Could not delete the author with id ${id} . cause: author not found`});
        } else {
            res.json(doc)
        }
    });
});

router.post("/salvar", (req, res) => {

    if (!req.body.autor) {
        res.status(502).send({error: 'A author must be sent in the body to save'});
        return;
    }

    let bodyAuthor = req.body.autor;
    let newAuthor = new Autor;

    if (!bodyAuthor.nome || !bodyAuthor.fotoUrl) {
        res.status(502).send({error: 'name and photo are required'});
        return;
    }

    if (bodyAuthor.obras) {
        newAuthor.obras.push(bodyAuthor.obras);
    }

    newAuthor.nome = bodyAuthor.nome;
    newAuthor.fotoUrl = bodyAuthor.fotoUrl;
    newAuthor.genero = bodyAuthor.genero;

    newAuthor.save((error, doc) => {
        if (error) {
            res.status(400).send({error: "Could not save the new author. Cause: " + error});
            return;
        }

        res.json(doc);
    });
});

router.put("/atualizar/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).send({error: 'it is necessary to send an id to update a author'});
        return;
    }

    if (!req.body.autor) {
        res.status(502).send({error: 'No author parameter was sent in the body to be updated'});
        return;
    }

    Autor.findOneAndUpdate({_id: id}, req.body.autor, function (error, doc) {
        if (error) {
            res.status(400).send({error: `Could not update the author with id ${id} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `Could not update the author with id ${id} . cause: author not found`});
        } else {
            res.json(doc);
        }
    })
});

module.exports = router;
