const express = require('express');
const router = express.Router();
const Dublador = require('../models/DubladorModel');


router.get("/", (req, res) => {
    var skip = 0;
    var limit = 10;
    var filter = {};

    if (req.query.skip) {
        skip = Number(req.query.skip);
    }

    if (req.query.limit) {
        limit = Number(req.query.limit);
    }

    if (req.query.nome) {
        filter.nome = {$regex: req.query.nome, $options: 'i'};
    }

    if (req.query.descricao) {
        filter.descricao = {$regex: req.query.descricao, $options: 'i'};
    }

    if (req.query.fotoUrl) {
        filter.fotoUrl = req.query.fotoUrl;
    }

    if (req.query.genero) {
        filter.genero = req.query.genero;
    }

    Dublador.find(filter, 'nome descricao fotoUrl genero personagens', {skip: skip, limit: limit}, (error, docs) => {

        if (error) {
            res.status(400).send({error: `Could not find the actors. cause: ${error}`});
            return;
        }

        if (docs == null) {
            res.status(400).send({error: `Could not find the actors. cause: actor not found`});
        } else {
            res.json(docs);
        }
    });
});

router.get("/:id", (req, res) => {
    var actorId = req.params.id;

    if (!actorId) {
        res.status(502).send({error: `id is required`});
        return;
    }

    Dublador.findById(actorId, (error, doc) => {
        if (error) {
            res.status(400).send({error: `Could not find the actor with id ${actorId} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `Could not find the actor with id ${actorId} . cause: actor not found`});
        } else {
            res.json(doc);
        }
    });
});

router.delete("/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).json({error: `id is required`});
        return;
    }

    Dublador.findOneAndDelete({_id: id}, function (error, doc) {

        if (error) {
            res.status(400).send({error: `Could not delete the actor with id ${id} . cause: ${error}`});
            return;
        }

        if(doc == null) {
            res.status(400).send({error: `Could not delete the actor with id ${id} . cause: actor not found`});
        }else {
            res.json(doc);
        }
    });
});

router.post("/salvar", (req, res) => {

    if (!req.body.dublador) {
        res.status(502).send({error: 'A actor must be sent in the body to save'});
        return;
    }

    let bodyActor = req.body.dublador;
    let newActor = new Dublador;

    if (!bodyActor.nome || !bodyActor.fotoUrl) {
        res.status(502).send({error: 'name and photo are required'});
        return;
    }

    if (bodyActor.personagens) {
        newActor.personagens.push(bodyActor.personagens);
    }

    newActor.nome = bodyActor.nome;
    newActor.descricao = bodyActor.descricao;
    newActor.fotoUrl = bodyActor.fotoUrl;
    newActor.genero = bodyActor.genero;

    newActor.save((error, doc) => {
        if (error) {
            res.status(400).send({error: "Could not save the new actor. Cause: " + error});
            return;
        }

        res.json(doc);
    });
});

router.put("/atualizar/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).send({error: 'it is necessary to send an id to update a actor'});
        return;
    }

    if (!req.body.dublador) {
        res.status(502).send({error: 'No actor parameter was sent in the body to be updated'});
        return;
    }

    Dublador.findOneAndUpdate({_id: id}, req.body.dublador, function (error, doc) {
        if (error) {
            res.status(400).send({error: `Could not update the actor with id ${id} . cause: ${error}`});
            return;
        }

        if(doc == null) {
            res.status(400).send({error: `Could not update the actor with id ${id} . cause: actor not found`});
        }else {
            res.json(doc);
        }
    });
});

module.exports = router;