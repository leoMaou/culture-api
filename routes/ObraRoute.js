const express = require('express');
const router = express.Router();
const Obra = require('../models/ObraModel');

router.get("/", (req, res) => {
    var skip = 0;
    var limit = 10;
    var filter = {};

    if (req.query.skip) {
        skip = Number(req.query.skip);
    }

    if (req.query.limit) {
        limit = Number(req.query.limit);
    }

    if (req.query.nome) {
        filter.nome = {$regex: req.query.nome, $options: 'i'};
    }

    if (req.query.descricao) {
        filter.descricao = {$regex: req.query.descricao, $options: 'i'};
    }

    if (req.query.fotoUrl) {
        filter.fotoUrl = req.query.fotoUrl;
    }

    if (req.query.status) {
        filter.status = req.query.status;
    }

    if (req.query.source) {
        filter.source = req.query.source;
    }

    if (req.query.formato) {
        filter.formato = req.query.formato;
    }

    if (req.query.genero) {
        filter.generos = [];
        filter.generos.push(req.query.genero);
    }

    if (req.query.studios) {
        filter.studios = [];
        filter.studios.push(req.query.studio);
    }

    Obra.find(filter, 'nome descricao fotoUrl status source formato generos  studios', {
        skip: skip,
        limit: limit
    }, (error, docs) => {

        if (error) {
            res.status(400).send({error: `Could not find the works. cause: ${error}`});
            return;
        }

        if (docs == null) {
            res.status(400).send({error: `Could not find the works. cause: work not found`});
        } else {
            res.json(docs);
        }
    });
});

router.get("/:id", (req, res) => {
    var obraId = req.params.id;

    if (!obraId) {
        res.status(502).send({error: `id is required`});
        return;
    }

    Obra.findById(obraId, (error, doc) => {
        if (error) {
            res.status(400).send({error: `Could not find the work with id ${obraId} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `Could not find the work with id ${obraId} . cause: work not found`});
        } else {
            res.json(doc);
        }
    });
});

router.delete("/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).json({error: `id is required`})
        return;
    }

    Obra.findOneAndDelete({_id: id}, function (error, doc) {

        if (error) {
            res.status(400).send({error: `Could not delete the work with id ${id} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `Could not delete the work with id ${id} . cause: work not found`});
        } else {
            res.json(doc);
        }
    });
});

router.post("/salvar", (req, res) => {

    if (!req.body.obra) {
        res.status(502).send({error: 'A work must be sent in the body to save'});
        return;
    }

    let bodyWork = req.body.obra;
    let newWork = new Obra;

    if (!bodyWork.nome || !bodyWork.descricao || !bodyWork.status) {
        res.status(502).send({error: 'name, description and status are required'});
        return;
    }

    newWork.nome = bodyWork.nome;
    newWork.descricao = bodyWork.descricao;
    newWork.fotoUrl = bodyWork.fotoUrl;
    newWork.status = bodyWork.status;
    newWork.source = bodyWork.source;
    newWork.formato = bodyWork.formato;
    newWork.generos = [];
    newWork.studios = [];

    if (bodyWork.generos) {
        newWork.generos = bodyWork.generos;
    }

    if (bodyWork.studios) {
        newWork.studios = bodyWork.studios;
    }

    newWork.save((error, doc) => {
        if (error) {
            res.status(400).send({error: "Could not save the new work. Cause" + error});
            return;
        }

        res.json(doc);
    });
});

router.put("/atualizar/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).send({error: 'it is necessary to send an id to update a work'});
        return;

    }

    if (!req.body.obra) {
        res.status(502).send({error: 'No work parameter was sent in the body to be updated'});
        return;
    }

    Obra.findOneAndUpdate({_id: id}, req.body.obra, function (error, doc) {
        if (error) {
            res.status(400).send({error: `Could not update the work with id ${id} . cause: ${error}`});
            return;
        }

        if(doc == null) {
            res.status(400).send({error: `Could not update the work with id ${id} . cause: work not found`});
        }else {
            res.json(doc);
        }
    });
});

module.exports = router;