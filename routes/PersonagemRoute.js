const express = require('express');
const router = express.Router();
const Personagem = require('../models/PersonagemModel');

router.get("/", (req, res) => {
    var skip = 0;
    var limit = 10;
    var filter = {};

    if (req.query.skip) {
        skip = Number(req.query.skip);
    }

    if (req.query.limit) {
        limit = Number(req.query.limit);
    }

    if (req.query.nome) {
        filter.nome = {$regex: req.query.nome, $options: 'i'};
    }

    if (req.query.descricao) {
        filter.descricao =  {$regex: req.query.descricao, $options: 'i'};
    }

    if (req.query.genero) {
        filter.genero = req.query.genero;
    }

    if (req.query.fotoUrl) {
        filter.fotoUrl = req.query.fotoUrl;
    }

    Personagem.find(filter, 'nome descricao fotoUrl genero obras', {skip: skip, limit: limit}, (error, docs) => {

        if (error) {
            res.status(400).send({error: `Could not find the characters. cause: ${error}`});
            return;
        }

        if (docs == null) {
            res.status(400).send({error: `Could not find the characters.`});
        } else {
            res.json(docs);
        }
    });
});

router.get("/:id", (req, res) => {
    var personagemId = req.params.id;

    if (!personagemId) {
        res.status(502).send({error: `id is required`});
        return;
    }

    Personagem.findById(personagemId, (error, doc) => {
        if (error) {
            res.status(400).send({error: `Could not find the character with id ${personagemId} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `Could not find the character with id ${personagemId}.`});
        } else {
            res.json(doc);
        }
    });
});

router.delete("/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).json({error: `id is required`});
        return;
    }

    Personagem.findOneAndDelete({_id: id}, function (error, doc) {

        if (error) {
            res.status(400).send({error: `Could not delete the character with id ${id} . cause: ${error}`});
            return;
        }

        if(id == null) {
            res.status(400).send({error: `Could not delete the character with id ${id}. character not found`});
        }else {
            res.json(doc)
        }
    });
});

router.post("/salvar", (req, res) => {

    if (!req.body.personagem) {
        res.status(502).send({error: 'A character must be sent in the body to save'});
        return;
    }

    let bodyCharacter = req.body.personagem;
    let newCharacter = new Personagem;

    if (!bodyCharacter.nome || !bodyCharacter.descricao || !bodyCharacter.fotoUrl) {
        res.status(502).send({error: 'name, description and url are required'});
        return;
    }

    if (bodyCharacter.obras) {
        newCharacter.obras.push(bodyCharacter.obras);
    }

    newCharacter.nome = bodyCharacter.nome;
    newCharacter.descricao = bodyCharacter.descricao;
    newCharacter.fotoUrl = bodyCharacter.fotoUrl;
    newCharacter.genero = bodyCharacter.genero;

    newCharacter.save((error, doc) => {
        if (error) {
            res.status(400).send({error: "Could not save the new character. Cause" + error});
            return;
        }

        res.json(doc);
    });
});

router.put("/atualizar/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).send({error: 'it is necessary to send an id to update a character'});
        return;
    }

    if (!req.body.personagem) {
        res.status(502).send({error: 'No character parameter was sent in the body to be updated'});
        return;
    }

    Personagem.findOneAndUpdate({_id: id}, req.body.personagem, function (error, doc) {
        if (error) {
            res.status(400).send({error: `Could not update the character with id ${id} . cause: ${error}`});
            return;
        }

        if(doc == null) {
            res.status(400).send({error: `Could not update the character with id ${id}. character not found`});
        }else {
            res.json(doc);
        }
    });
});

module.exports = router;
