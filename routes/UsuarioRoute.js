const express = require('express');
const router = express.Router();
const Usuario = require('../models/UsuarioModel');
var jwt = require('jsonwebtoken');

var SECRET = "AFDJDKLGHSLHJFLKDSLKFDKLHGFDS3421N";

router.get("/", (req, res) => {
    var skip = 0;
    var limit = 10;
    var filter = {};

    if (req.query.skip) {
        skip = Number(req.query.skip);
    }

    if (req.query.limit) {
        limit = Number(req.query.limit);
    }

    if (req.query.nome) {
        filter.nome = {$regex: req.query.nome, $options: 'i'};
    }

    if (req.query.senha) {
        filter.senha = req.query.senha;
    }

    if (req.query.login) {
        filter.login = req.query.login;
    }

    Usuario.find(filter, 'nome login senha favoritos', {skip: skip, limit: limit}, (error, docs) => {

        if (error) {
            res.status(400).send({error: `Could not find the users. cause: ${error}`});
            return;
        }

        if (docs == null) {
            res.status(400).send({error: `Could not find the users.`});
        } else {
            res.json(docs);
        }
    });
});

router.get("/:id", (req, res) => {
    var usuarioId = req.params.id;

    if (!usuarioId) {
        res.status(502).json({error: `id is required`});
        return;
    }

    Usuario.findById(usuarioId, (error, doc) => {
        if (error) {
            res.status(400).send({error: `Error at find the user with id ${usuarioId} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `Error at find the user with id ${usuarioId}.`});
        } else {
            res.json(doc);
        }
    });
});


router.delete("/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).json({error: `id is required`});
        return;
    }

    Usuario.findOneAndDelete({_id: id}, function (error, doc) {

        if (error) {
            res.status(400).send({error: `Could not delete the user with id ${id} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `Could not delete the user with id ${id} . cause: User not found`});
        } else {
            res.json(doc)
        }
    });
});

router.post("/login", (req, res) => {

    if (!req.body.usuario) {
        res.status(502).send({error: 'A user must be sent in the body to save'});
        return;
    }

    let bodyUser = req.body.usuario;

    if (!bodyUser.login || !bodyUser.senha) {
        res.status(502).send({error: 'login and password are required'});
        return;
    }

    Usuario.findOne({login: bodyUser.login, senha: bodyUser.senha}, (error, doc) => {

        if (error) {
            res.status(400).send({error: `error at login cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `login and password invalid, user not found`});
        } else {
            var token = jwt.sign({login: doc.login, senha: doc.senha}, SECRET, {
                expiresIn: "365 days" // expires in 1 year
            });

            res.json({usuario: doc, token: token});
        }
    });
});

router.post("/salvar", (req, res) => {

    if (!req.body.usuario) {
        res.status(502).send({error: 'A user must be sent in the body to save'});
        return;
    }

    let bodyUser = req.body.usuario;
    let newUser = new Usuario;

    if (!bodyUser.login || !bodyUser.senha) {
        res.status(502).send({error: 'login and password are required'});
        return;
    }

    //todo (emerson) fazer validação se login ja existe

    if (bodyUser.nome) {
        newUser.nome = bodyUser.nome;
    }

    newUser.login = bodyUser.login;
    newUser.senha = bodyUser.senha;

    newUser.save((error, doc) => {
        if (error) {
            res.status(400).send({error: "Could not save the new user. Cause" + error});
        }


        res.json(doc);
    });
});

router.put("/atualizar/:id", (req, res) => {
    var id = req.params.id;

    if (!id) {
        res.status(502).send({error: 'it is necessary to send an id to update a user'});
        return;
    }

    if (!req.body.usuario) {
        res.status(502).send({error: 'No user parameter was sent in the body to be updated'});
        return;
    }

    Usuario.findOneAndUpdate({_id: id}, req.body.usuario, function (error, doc) {
        if (error) {
            res.status(400).send({error: `Could not update the user with id ${id} . cause: ${error}`});
            return;
        }

        if (doc == null) {
            res.status(400).send({error: `no user found with id ${id} cause: User not found`});
        } else {
            res.json(doc);
        }
    });
});


module.exports = router;
